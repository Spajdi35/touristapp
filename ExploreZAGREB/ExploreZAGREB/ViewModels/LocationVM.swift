//
//  LocationVM.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 16/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import RxSwift

struct LocationListVM {
    let locations: [LocationVM]
    
    init(locations: [Location]) {
        self.locations = locations.compactMap({ (location) -> LocationVM? in
            return LocationVM(location: location)
        })
    }
    
}

struct LocationVM {
    let location: Location
    
    init(location: Location) {
        self.location = location
    }
    
}
