//
//  CategoryVM.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 18/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import RxSwift

struct CategoryListVM {
    let categories: [CategoryVM]
    
    init(categories: [Category]) {
        self.categories = categories.compactMap({ (category) -> CategoryVM? in
            return CategoryVM(category: category)
        })
    }
    
    func count() -> Int {
        return categories.count
    }
    
}

struct CategoryVM {
    let category: Category
    
    init(category: Category) {
        self.category = category
    }
    
    var title: Observable<String> {
        return Observable<String>.just(category.name.uppercased())
    }
    
    var image: Observable<UIImage> {
        return Observable<UIImage>.just(UIImage(named: category.name)!)
    }
    
}
