//
//  RouteModel.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 18/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation

struct Route: Decodable {
    let id: Int
    let locations: [Location]
}
