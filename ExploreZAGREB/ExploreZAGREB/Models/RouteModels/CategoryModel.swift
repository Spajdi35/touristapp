//
//  CategoryModel.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 08/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import UIKit

struct CategoryResponse: Decodable {
    let categories: [Category]
}

struct Category: Decodable {
    let id: Int
    let name: String
//    let routes: [Route]
}
