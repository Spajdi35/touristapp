//
//  Location.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 15/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation

struct Location: Decodable {
    let id: Int
    let latitude: Double
    let longitude: Double
    let address: String
    let description: String?
}
