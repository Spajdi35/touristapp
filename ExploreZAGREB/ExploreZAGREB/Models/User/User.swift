//
//  UserModel.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 09/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation

struct User: Decodable {
    var id: Int?
    let firstName: String
    let lastName: String
    let email: String
    let birthday: String
    let country: String
    
    init(firstName: String, lastName: String, email: String, birthday: String, country: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.birthday = birthday
        self.country = country
    }
    
    func toJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["firstName"] = self.firstName
        json["lastName"] = self.lastName
        json["email"] = self.email
        json["birthday"] = self.birthday
        json["country"] = self.country
        
        return json
    }
    
}
