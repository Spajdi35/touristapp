//
//  AppService.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 09/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AppService {
    // online database connection string
//    private static let connectionString = "https://morning-cliffs-28502.herokuapp.com"
    private static let connectionString = "http://localhost:8080"
    
    static func registerUser(user: User) {
        self.post(urlString: connectionString + "/user", parameters: user.toJson())
    }
    
    static func getAllCategories() -> Observable<[Category]> {
        return self.load(class: [Category].self, urlString: connectionString + "/category/getAll")
    }
    
    static func getAllLocations() -> Observable<[Location]> {
        return self.load(class: [Location].self, urlString: connectionString + "/location/getAll")
    }
    
    static func getCategoryByName(name: String) -> Observable<Category> {
        return self.load(class: Category.self, urlString: connectionString + "/category/get/\(name)")
    }
    
    static func getRoutesByCategoryId(id: Int) -> Observable<[Route]> {
        return self.load(class: [Route].self, urlString: connectionString + "/route/getByCategoryId/\(id)")
    }
    
    static func userExists(username: String) -> Observable<User> {
        return self.load(class: User.self, urlString: connectionString + "/user/getByUsername/\(username)")
    }
    
    static func getUserByEmail(email: String) -> Observable<User> {
        return self.load(class: User.self, urlString: connectionString + "/user/getByEmail/\(email)")
    }
    
    static func getAllUsers() -> Observable<[User]> {
        return self.load(class: [User].self, urlString: connectionString + "/user/getAll")
    }
    
}


extension AppService {
    
    private static func load<T: Decodable>(class: T.Type, urlString: String) -> Observable<T> {
        let url = URL(string: urlString)!
        
        return Observable.from([url]).flatMap({ (url) -> Observable<Data> in
            let request = URLRequest(url: url)
            return URLSession.shared.rx.data(request: request)
        }).map({ (data) -> T in
            return try JSONDecoder().decode(T.self, from: data)
        }).asObservable()
    }
    
    
    private static func post(urlString: String, parameters: [String: Any]) {
        let url = URL(string: urlString)!
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
}

