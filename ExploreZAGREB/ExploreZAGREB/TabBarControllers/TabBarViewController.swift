//
//  TabBarViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 08/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    var mapVC: MapViewController!
    var categoryVC: TESTINGCategoryViewController!
//    var categoryVC: CategoryCollectionViewController!
    var locationVC: LocationViewController!
    var profileVC: ProfileViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        self.tabBar.unselectedItemTintColor = UIColor(hexString: "B0B0B0")
        
        self.view.backgroundColor = .white
        self.navigationItem.hidesBackButton = true
        
        setupControllers()

        // -- Tasks with controllers goes here
    }
    
    private func setupControllers() {
        mapVC = MapViewController()
        mapVC.tabBarItem = UITabBarItem(title: "Map", image: UIImage(named: "map@1"), tag: 0)
        
//        categoryVC = CategoryCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout.init())
        categoryVC = TESTINGCategoryViewController()
        categoryVC.tabBarItem = UITabBarItem(title: "Routes", image: UIImage(named: "29"), tag: 1)
        
        locationVC = LocationViewController()
        locationVC.tabBarItem = UITabBarItem(title: "Locations", image: UIImage(named: "Location"), tag: 2)
        
        profileVC = ProfileViewController()
        profileVC.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), tag: 3)
        
        let navMapVC = UINavigationController(rootViewController: mapVC)
        let navCategoryVC = UINavigationController(rootViewController: categoryVC)
        let navLocationVc = UINavigationController(rootViewController: locationVC)
        let navProfileVC = UINavigationController(rootViewController: profileVC)
        
        let tabBarList = [navMapVC, navCategoryVC, navLocationVc, navProfileVC]
        self.viewControllers = tabBarList
    }

}


extension TabBarViewController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
            return false // Make sure you want this as false
        }
        
        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        }
        
        return true
    }
}
