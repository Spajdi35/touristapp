//
//  Slide.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 11/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift

class Slide: UIView {
    
    let disposeBag = DisposeBag()

    var imageView: UIImageView!
    var categoryLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Coder not implemented")
    }
    
    func setUI() {
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        
        categoryLabel = UILabel()
        addSubview(categoryLabel)
        
        setConstraints()
    }

    func setConstraints() {
        imageView.autoPinEdge(toSuperviewEdge: .top)
        imageView.autoPinEdge(toSuperviewEdge: .left)
        imageView.autoPinEdge(toSuperviewEdge: .right)
        imageView.autoPinEdge(toSuperviewEdge: .bottom, withInset: UITabBar.appearance().frame.height)
    }
    
}
