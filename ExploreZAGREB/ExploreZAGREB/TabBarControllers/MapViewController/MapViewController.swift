//
//  MapViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 08/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa

class MapViewController: UIViewController, MKMapViewDelegate {
    
    let disposeBag = DisposeBag()
    
    var mapView: MKMapView!
    
    private var locations: [Location]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.navigationController?.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: nil)

        setUI()
        
        mapView.delegate = self
        initialMapState()
        
        AppService.getAllLocations().subscribe { [weak self] (locationResponse) in
            if let locations = locationResponse.element {
                self?.locations = locations
                self?.addAnnotations()
            }
        }.disposed(by: disposeBag)
        
    }
    
    private func initialMapState() {
        // starting map view located on Zagreb
        let initialLocation = CLLocation(latitude: 45.815399, longitude: 15.966568)
        let regionRadius: CLLocationDistance = 1000
            
        let coordinateRegion = MKCoordinateRegion(center: initialLocation.coordinate,
                                                      latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func addAnnotations() {
        var annotations = [MKAnnotation]()
        
        self.locations?.forEach({ (location) in
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            annotation.title = location.address
            
            annotations.append(annotation)
        })
        
        DispatchQueue.main.async {
            self.mapView.addAnnotations(annotations)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

}


extension MapViewController {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation = view.annotation
        
        self.mapView.deselectAnnotation(annotation, animated: true)
    }
    
}

