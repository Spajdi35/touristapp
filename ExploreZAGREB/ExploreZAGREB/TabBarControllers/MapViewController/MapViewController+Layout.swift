//
//  MapViewController+Layout.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 08/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import MapKit

extension MapViewController {
    
    func setUI() {
        mapView = MKMapView()
        self.view.addSubview(mapView)
        
        setConstraints()
    }
    
    func setConstraints() {
        mapView.autoPinEdgesToSuperviewEdges()
    }
    
}
