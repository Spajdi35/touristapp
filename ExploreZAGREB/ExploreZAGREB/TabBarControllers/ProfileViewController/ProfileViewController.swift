//
//  ProfileViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 11/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import PureLayout
import RxSwift

class ProfileViewController: UIViewController {
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        let logoutBtn = UIButton()
        logoutBtn.setTitle("Logout", for: .normal)
        logoutBtn.backgroundColor = UIColor.blue
        logoutBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        self.view.addSubview(logoutBtn)
        
        logoutBtn.autoSetDimensions(to: CGSize(width: 200, height: 80))
        
        logoutBtn.autoAlignAxis(toSuperviewAxis: .horizontal)
        logoutBtn.autoAlignAxis(toSuperviewAxis: .vertical)
        
        logoutBtn.rx.tap.subscribe(onNext: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            guard let window = appDelegate.window else { return }
            let navVC = UINavigationController()
            let loginView = LoginViewController(nibName: nil, bundle: nil)
            navVC.viewControllers = [loginView]
            
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                window.rootViewController = navVC
            }, completion: { completed in
                // maybe do something here
            })
            
        }).disposed(by: disposeBag)
        
    }
    

}
