//
//  RouteCollectionViewCell.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 18/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import PureLayout
import ChameleonFramework

class CategoryCollectionViewCell: UICollectionViewCell {
    
    var imageView: UIImageView!
    var nameLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.cornerRadius = 5.0
        self.backgroundColor = UIColor.flatSkyBlue
        
        setUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUI() {
        
        imageView = UIImageView()
        imageView.backgroundColor = UIColor.flatSkyBlue
        imageView.contentMode = .scaleAspectFit
        self.contentView.addSubview(imageView)
        
        nameLabel = UILabel()
        nameLabel.backgroundColor = UIColor.clear
        nameLabel.textColor = UIColor.white
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        self.contentView.addSubview(nameLabel)
    
        setConstraints()
    }
    
    func setConstraints() {
        imageView.autoPinEdge(toSuperviewEdge: .top, withInset: 10)
        imageView.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
        imageView.autoPinEdge(toSuperviewEdge: .right, withInset: 10)
        imageView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 30)
        
        nameLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 5)
        nameLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 5)
        nameLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 3)
//        nameLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 30)
    }
    
}
