//
//  RoutesViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 20/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift

class RoutesViewController: UIViewController {
    
    var category: Category!
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        AppService.getRoutesByCategoryId(id: category.id).subscribe { (routesResponse) in
            if let routes = routesResponse.element {
                print(routes)
            }
        }.disposed(by: disposeBag)
    }
    
    init(category: Category) {
        self.category = category
        super.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
