//
//  RoutesViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 17/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift

class CategoryCollectionViewController: UICollectionViewController {
    
    var categoryListVM: CategoryListVM?
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.backgroundColor = .white
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(CategoryCollectionViewCell.self, forCellWithReuseIdentifier: "identifier")
        
        AppService.getAllCategories().subscribe { (categoriesResponse) in
            if let categories = categoriesResponse.element {
                self.categoryListVM = CategoryListVM(categories: categories)
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }.disposed(by: disposeBag)
        
        setUI()
    }
    
    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


extension CategoryCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let categoryListVM = self.categoryListVM {
            return categoryListVM.count()
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifier", for: indexPath) as! CategoryCollectionViewCell
        
        let categoryVM = self.categoryListVM?.categories[indexPath.row]
        
        categoryVM?.title.asDriver(onErrorJustReturn: "")
        .drive(cell.nameLabel.rx.text)
        .disposed(by: disposeBag)
        
        // need rx way to do this
        
        Observable<UIImage>.just(UIImage(named: (categoryVM?.category.name)!)!).asDriver(onErrorJustReturn: UIImage())
        .drive(cell.imageView.rx.image)
        .disposed(by: disposeBag)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.frame.width - 4) / 2, height: (view.frame.width - 4) / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let categoryVM = self.categoryListVM?.categories[indexPath.row]
        
        if let category = categoryVM?.category {
            let routesVC = RoutesViewController(category: category)
            self.navigationController?.pushViewController(routesVC, animated: true)
        }
        
    }
    
}
