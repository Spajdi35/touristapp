//
//  TESTINGCategoryViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 11/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift

class TESTINGCategoryViewController: UIViewController, UIScrollViewDelegate {
    
    let disposeBag = DisposeBag()
    
    var scrollView: UIScrollView!
    var pageControl: UIPageControl!
    
    var categoryListVM: CategoryListVM?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        
        self.scrollView.backgroundColor = UIColor(hexString: "1F4F87")
        
        AppService.getAllCategories().subscribe { [weak self] (categoriesResponse) in
            if let categories = categoriesResponse.element {
                self?.categoryListVM = CategoryListVM(categories: categories)
                self?.setupSlideScrollView()
            }
        }.disposed(by: disposeBag)
        
        scrollView.delegate = self
    }
    
    func setUI() {
        scrollView = UIScrollView()
        view.addSubview(scrollView)
        
        pageControl = UIPageControl()
        pageControl.backgroundColor = .black
        view.addSubview(pageControl)
        view.bringSubviewToFront(pageControl)
        
        setConstraints()
    }
    
    func setConstraints() {
        scrollView.autoPinEdgesToSuperviewEdges()
        
        pageControl.autoSetDimension(.height, toSize: 40)
        pageControl.autoSetDimension(.width, toSize: 80)
        pageControl.autoPinEdge(toSuperviewEdge: .bottom, withInset: 100)
        pageControl.autoAlignAxis(.vertical, toSameAxisOf: view)
    }
    
    
    
    func setupSlideScrollView() {
        
        DispatchQueue.main.async {
            var slides = self.createSlides()
            
            self.scrollView.contentSize = CGSize(width: self.view.frame.width * CGFloat(slides.count), height: self.view.frame.height)
            self.scrollView.isPagingEnabled = true
            
            self.pageControl.numberOfPages = slides.count
            self.pageControl.currentPage = 0
            
            for i in 0 ..< slides.count {
                slides[i].frame = CGRect(x: self.view.frame.width * CGFloat(i), y: 0, width: self.view.frame.width, height: self.view.frame.height)
                self.scrollView.addSubview(slides[i])
                print("dodajem")
            }
        }
        
        
    }
    
    func createSlides() -> [Slide] {
        
        var slides: [Slide] = []
        
        categoryListVM?.categories.forEach({ (categoryVM) in
            let slide = Slide()
            
            categoryVM.image.asDriver(onErrorJustReturn: UIImage())
                .drive(slide.imageView.rx.image)
                .disposed(by: self.disposeBag)
            
            slides.append(slide)
        })
        
        return slides
    }
    
}
