//
//  LocationTableTableViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 23/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift

class LocationViewController: UIViewController {
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        AppService.getAllLocations().subscribe { (locationsResponse) in
            if let locations = locationsResponse.element {
                print(locations)
            }
        }.disposed(by: disposeBag)
        
    }
 
}
