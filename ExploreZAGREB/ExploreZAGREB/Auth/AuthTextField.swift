//
//  AuthTextField.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 18/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import UIKit

class AuthTextField: UITextField, UITextFieldDelegate {
    
    init(frame: CGRect, placeholder: String) {
        super.init(frame: frame)
        delegate = self
        style(placeholder: placeholder)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("coder not implemented")
    }
    
    private func style(placeholder: String) {
        self.layer.cornerRadius = 15
        self.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "686262") ?? .gray])
        self.setLeftPaddingPoints(18)
        self.backgroundColor = UIColor.white.withAlphaComponent(0.8)
    }
    
}
