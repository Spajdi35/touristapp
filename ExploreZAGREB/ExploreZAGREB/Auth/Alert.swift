//
//  Alert.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 17/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import UIKit

class Alert: UIAlertController {
    
    init(title: String, message: String, prefferedStyle: UIAlertController.Style) {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
