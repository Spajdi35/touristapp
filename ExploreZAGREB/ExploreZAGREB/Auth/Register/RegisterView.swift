//
//  RegisterView.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 07/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import CountryPicker
import RxSwift
import Firebase

class RegisterView: UIView, UIScrollViewDelegate {
    
    let disposeBag = DisposeBag()

    var imageView: UIImageView!
    var scrollView: UIScrollView!
    
    var firstNameTextField: UITextField!
    var lastNameTextField: UITextField!
    var emailTextField: UITextField!
    var passwordTextField: UITextField!
    var repeatPasswordTextField: UITextField!
    
    var birthDateTextField: UITextField!
    var datePicker: UIDatePicker!
    
    var countryPicker: CountryPicker!
    
    var signUpBtn: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
        
        scrollView.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Coder not implemented")
    }
    
    func setUI() {
        imageView = UIImageView()
        imageView.image = UIImage(named: "konj")
        imageView.contentMode = .scaleAspectFill
        addSubview(imageView)
        
        scrollView = UIScrollView.newAutoLayout()
        scrollView.backgroundColor = UIColor(hexString: "C4C4C4")?.withAlphaComponent(0.67)
        scrollView.layer.cornerRadius = 40
//        scrollView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollView)
        
        firstNameTextField = AuthTextField(frame: CGRect.zero, placeholder: "First name")
        scrollView.addSubview(firstNameTextField)
        
        lastNameTextField = AuthTextField(frame: CGRect.zero, placeholder: "Last name")
        scrollView.addSubview(lastNameTextField)
        
        emailTextField = AuthTextField(frame: CGRect.zero, placeholder: "Email")
        scrollView.addSubview(emailTextField)
        
        passwordTextField = AuthTextField(frame: CGRect.zero, placeholder: "Password")
        passwordTextField.isSecureTextEntry = true
        scrollView.addSubview(passwordTextField)
        
        repeatPasswordTextField = AuthTextField(frame: CGRect.zero, placeholder: "Repeat password")
        repeatPasswordTextField.isSecureTextEntry = true
        scrollView.addSubview(repeatPasswordTextField)
        
        birthDateTextField = AuthTextField(frame: CGRect.zero, placeholder: "Birth date")
        scrollView.addSubview(birthDateTextField)
        
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        
        birthDateTextField.inputView = datePicker
        
        countryPicker = CountryPicker()
        countryPicker.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        countryPicker.layer.cornerRadius = 15
        scrollView.addSubview(countryPicker)
        
        signUpBtn = UIButton()
        signUpBtn.layer.cornerRadius = 34
        signUpBtn.backgroundColor = UIColor(hexString: "1C5182")?.withAlphaComponent(0.9)
        signUpBtn.setTitle("Sign up", for: .normal)
        signUpBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        scrollView.addSubview(signUpBtn)
        
        setConstraints()
    }
    
    func setConstraints() {
        imageView.autoPinEdgesToSuperviewEdges()
        
        scrollView.autoPinEdge(toSuperviewEdge: .top, withInset: 15)
        scrollView.autoPinEdge(toSuperviewEdge: .left, withInset: 5)
        scrollView.autoPinEdge(toSuperviewEdge: .right, withInset: 5)
        scrollView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 15)
        
        firstNameTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 30)
        firstNameTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 26)
        firstNameTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 26)
        firstNameTextField.autoSetDimension(.height, toSize: 50)
        
        lastNameTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 110)
        lastNameTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 26)
        lastNameTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 26)
        lastNameTextField.autoSetDimension(.height, toSize: 50)
        
        emailTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 190)
        emailTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 26)
        emailTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 26)
        emailTextField.autoSetDimension(.height, toSize: 50)
        
        passwordTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 270)
        passwordTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 26)
        passwordTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 26)
        passwordTextField.autoSetDimension(.height, toSize: 50)
        
        repeatPasswordTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 350)
        repeatPasswordTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 26)
        repeatPasswordTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 26)
        repeatPasswordTextField.autoSetDimension(.height, toSize: 50)
        
        birthDateTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 430)
        birthDateTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 26)
        birthDateTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 26)
        birthDateTextField.autoSetDimension(.height, toSize: 50)
        
        countryPicker.autoPinEdge(toSuperviewEdge: .top, withInset: 510)
        countryPicker.autoPinEdge(toSuperviewEdge: .left, withInset: 26)
        countryPicker.autoPinEdge(toSuperviewEdge: .right, withInset: 26)
        countryPicker.autoSetDimension(.height, toSize: 150)
        
        signUpBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 690)
        signUpBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 18)
        signUpBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 18)
        signUpBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 20)
        signUpBtn.autoSetDimension(.height, toSize: 70)
    }
    
    
    func resetTextFields() {
        DispatchQueue.main.async {
            self.firstNameTextField.text = ""
            self.lastNameTextField.text = ""
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
            self.repeatPasswordTextField.text = ""
            self.firstNameTextField.becomeFirstResponder()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 || scrollView.contentOffset.x<0 {
            print("tu sam")
            scrollView.contentOffset.x = 0
        }
    }

}

extension Date {
    func toString( dateFormat format  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
