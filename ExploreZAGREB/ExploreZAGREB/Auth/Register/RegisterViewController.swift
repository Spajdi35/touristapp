//
//  RegisterViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 30/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift

class RegisterViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    let registerView = RegisterView(frame: .zero)
    
    var registerStrategy: RegisterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Login", style: UIBarButtonItem.Style.plain, target: self, action: #selector(RegisterViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.view.addSubview(registerView)
        registerView.autoPinEdgesToSuperviewEdges()
        
        registerView.signUpBtn.rx.tap.subscribe(onNext: { [weak self] in
            
            if let email = self?.registerView.emailTextField.text, let pass = self?.registerView.passwordTextField.text, let repeatPass = self?.registerView.repeatPasswordTextField.text {
                
                guard let firstName = self?.registerView.firstNameTextField.text else { return }
                guard let lastName = self?.registerView.lastNameTextField.text else { return }
                guard let date = self?.registerView.datePicker.date else { return }
                guard let country = self?.registerView.countryPicker.selectedCountryName else { return }
                
                if pass == repeatPass {
                    let user = User(firstName: firstName, lastName: lastName, email: email.lowercased(), birthday: date.toString(dateFormat: "dd.MM.yyyy."), country: country)
                    self?.registerStrategy = FirebaseRegister()
                    self?.registerStrategy.register(user: user, password: pass, completion: { (err) in
                        
                        if err != nil {
                            let wrongCredentialsAlertController = UIAlertController(title: "We can't register you", message:"Something went wrong", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) in
                                self?.registerView.resetTextFields()
                            })
                            
                            wrongCredentialsAlertController.addAction(alertAction)
                            self?.present(wrongCredentialsAlertController, animated: true, completion: nil)
                        } else {
                            let okAlertController = UIAlertController(title: "Registration successful!", message: "Verification link has been sent to your email", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                //// custom navVC animation
                                let transition = CATransition()
                                transition.duration = 0.5
                                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                                transition.type = CATransitionType.fade
                                self?.navigationController?.view.layer.add(transition, forKey: nil)
                                
                                self?.navigationController?.popToRootViewController(animated: false)
                            })
                            
                            okAlertController.addAction(alertAction)
                            self?.present(okAlertController, animated: true, completion: nil)
                        }
                        
                    })
                } else {
                    let wrongCredentialsAlertController = UIAlertController(title: "We can't register you", message: "Passwords doesn't match", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) in
                        self?.registerView.resetTextFields()
                    })
                    
                    wrongCredentialsAlertController.addAction(alertAction)
                    self?.present(wrongCredentialsAlertController, animated: true, completion: nil)
                }
            }
            
        }).disposed(by: disposeBag)
        
        // height calculated for this based on constraints... ?
        registerView.scrollView.contentSize = CGSize(width: registerView.scrollView.frame.width, height: 690)
        
        registerView.datePicker.addTarget(self, action: #selector(dataChanged(datePicker:)), for: .valueChanged)
    }
    
    @objc func back(sender: UIBarButtonItem) {
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @objc func dataChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        registerView.birthDateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = UIColor(hexString: "1C5182")?.withAlphaComponent(0.9)
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    

}
