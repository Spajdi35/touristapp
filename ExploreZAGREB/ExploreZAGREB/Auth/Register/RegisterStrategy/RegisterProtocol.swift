//
//  RegisterProtocol.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 16/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation

// created because someday in future we may use some other (free) provider
protocol RegisterProtocol {
    func register(user: User, password: String, completion: @escaping (Error?) -> ())
}
