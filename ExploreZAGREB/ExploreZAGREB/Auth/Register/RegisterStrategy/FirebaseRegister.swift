//
//  File.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 16/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import Firebase

class FirebaseRegister: RegisterProtocol {
    
    func register(user: User, password: String, completion: @escaping (Error?) -> ()) {
        Auth.auth().createUser(withEmail: user.email, password: password, completion: { (response, err) in
            print("Registration error: \(String(describing: err?.localizedDescription))")
            
            if err == nil {
                Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                    print("Verification email error: \(String(describing: error?.localizedDescription))")
                })
                AppService.registerUser(user: user)
                completion(err)
            } else {
                completion(err)
            }
        })
    }
    
}
