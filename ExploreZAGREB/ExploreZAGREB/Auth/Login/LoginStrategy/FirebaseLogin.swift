//
//  FirebaseLogin.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 16/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation
import Firebase
import UIKit

class FirebaseLogin: LoginProtocol {
    
    func login(email: String, password: String, completion: @escaping (Error?) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                completion(error)
                print("greska greska")
            } else {
                let tabBarVC = TabBarViewController()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                if let window = appDelegate.window {
                    UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                        window.rootViewController = tabBarVC
                    }, completion: { completed in
                        
                    })
                }
            }
            
        })
    }
    
}
