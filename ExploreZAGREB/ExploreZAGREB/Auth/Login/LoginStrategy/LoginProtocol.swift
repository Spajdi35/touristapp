//
//  LoginProtocol.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 16/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import Foundation

protocol LoginProtocol {
    func login(email: String, password: String, completion: @escaping (Error?) -> ())
}
