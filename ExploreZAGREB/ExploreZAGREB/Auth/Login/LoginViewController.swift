//
//  Login2ViewController.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 29/08/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Firebase

class LoginViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    let loginView = LoginView(frame: .zero)
    
    var handle: AuthStateDidChangeListenerHandle?
    
    var loginStrategy: LoginProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        self.view.addSubview(loginView)
        loginView.autoPinEdgesToSuperviewEdges()
        
        loginView.loginBtn.rx.tap.subscribe(onNext: { [weak self] in
            
            if let email = self?.loginView.usernameTextField.text, let pass = self?.loginView.passwordTextField.text {
                self?.loginStrategy = FirebaseLogin()
                self?.loginStrategy.login(email: email, password: pass, completion: { (err) in
                    
                    if err != nil {
                        let wrongCredentialsAlertController = UIAlertController(title: "Wrong email or password", message: "Please try again", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) in
                            DispatchQueue.main.async {
                                self?.loginView.usernameTextField.text = ""
                                self?.loginView.passwordTextField.text = ""
                                self?.loginView.usernameTextField.becomeFirstResponder()
                            }
                        })
                        wrongCredentialsAlertController.addAction(alertAction)
                        self?.present(wrongCredentialsAlertController, animated: true, completion: nil)
                    }
                    
                })
            }
                
        }).disposed(by: disposeBag)
        
        loginView.registerButton.rx.tap.subscribe(onNext: {
            let registerVC = RegisterViewController()
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.fade
            self.navigationController?.view.layer.add(transition, forKey: nil)
            
            self.navigationController?.pushViewController(registerVC, animated: false)
        }).disposed(by: disposeBag)
    }
    
    
    // -- COMMENTED WHILE TESTED
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
        loginView.container.alpha = 0
        
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 2.5) {
            self.loginView.container.alpha = 1
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Auth.auth().removeStateDidChangeListener(handle!)
    }


}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
