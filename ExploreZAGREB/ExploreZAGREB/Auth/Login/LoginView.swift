//
//  LoginView.swift
//  ExploreZAGREB
//
//  Created by Zeljko halle on 07/09/2019.
//  Copyright © 2019 Zeljko halle. All rights reserved.
//

import UIKit
import RxSwift
import Firebase

class LoginView: UIView {
    
    let disposeBag = DisposeBag()
    
    var imageView: UIImageView!
    var container: UIView!
    var usernameTextField: UITextField!
    var passwordTextField: UITextField!
    var loginBtn: UIButton!
    var registerButton: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Coder not implemented")
    }
    
    func setUI() {
        self.imageView = UIImageView()
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.image = UIImage(named: "konj")
        addSubview(imageView)
        
        self.container = UIView()
        self.container.layer.cornerRadius = 40
        self.container.backgroundColor = UIColor(hexString: "C4C4C4")?.withAlphaComponent(0.67)
        addSubview(container)
        
        usernameTextField = AuthTextField(frame: CGRect.zero, placeholder: "Email")
        container.addSubview(usernameTextField)
        
        passwordTextField = AuthTextField(frame: CGRect.zero, placeholder: "Password")
        passwordTextField.isSecureTextEntry = true
        container.addSubview(passwordTextField)
        
        loginBtn = UIButton()
        loginBtn.layer.cornerRadius = 34
        loginBtn.backgroundColor = UIColor(hexString: "1C5182")?.withAlphaComponent(0.9)
        loginBtn.setTitle("Login", for: .normal)
        loginBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        container.addSubview(loginBtn)
        
        registerButton = UIButton()
        registerButton.setTitle("Don't have an account yet? Sign up now", for: .normal)
        registerButton.titleLabel?.lineBreakMode = .byWordWrapping
        registerButton.titleLabel?.textAlignment = .center
        registerButton.setTitleColor(UIColor(hexString: "686262"), for: .normal)
        container.addSubview(registerButton)
        
        setConstraints()
    }
    
    func setConstraints() {
        imageView.autoPinEdgesToSuperviewEdges()
        
        container.autoSetDimension(.height, toSize: 371)
        container.autoPinEdge(toSuperviewEdge: .left, withInset: 25)
        container.autoPinEdge(toSuperviewEdge: .right, withInset: 25)
        container.autoPinEdge(toSuperviewEdge: .bottom, withInset: 44)
        
        usernameTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 38)
        usernameTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 23)
        usernameTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 23)
        usernameTextField.autoPinEdge(toSuperviewEdge: .bottom, withInset: 286)
        
        passwordTextField.autoPinEdge(.top, to: .bottom, of: usernameTextField, withOffset: 30)
        passwordTextField.autoPinEdge(toSuperviewEdge: .left, withInset: 23)
        passwordTextField.autoPinEdge(toSuperviewEdge: .right, withInset: 23)
        passwordTextField.autoPinEdge(toSuperviewEdge: .bottom, withInset: 209)
        
        loginBtn.autoPinEdge(.top, to: .bottom, of: passwordTextField, withOffset: 52)
        loginBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 18)
        loginBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 18)
        loginBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 87)
        
        registerButton.autoPinEdge(.top, to: .bottom, of: loginBtn, withOffset: 19)
        registerButton.autoPinEdge(toSuperviewEdge: .left, withInset: 39)
        registerButton.autoPinEdge(toSuperviewEdge: .right, withInset: 39)
        registerButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: 20)
    }
    
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
