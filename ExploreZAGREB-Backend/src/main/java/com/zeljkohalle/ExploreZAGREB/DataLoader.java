package com.zeljkohalle.ExploreZAGREB;

import com.zeljkohalle.ExploreZAGREB.dao.CategoryRepository;
import com.zeljkohalle.ExploreZAGREB.dao.LocationRepository;
import com.zeljkohalle.ExploreZAGREB.dao.RouteRepository;
import com.zeljkohalle.ExploreZAGREB.dao.UserRepository;
import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Location;
import com.zeljkohalle.ExploreZAGREB.domain.Route;
import com.zeljkohalle.ExploreZAGREB.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

    private UserRepository userRepository;
    private LocationRepository locationRepository;
    private RouteRepository routeRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public DataLoader(UserRepository userRepository, LocationRepository locationRepository, RouteRepository routeRepository, CategoryRepository categoryRepository) {
        this.userRepository = userRepository;
        this.locationRepository = locationRepository;
        this.routeRepository = routeRepository;
        this.categoryRepository = categoryRepository;
    }

    // testing porposes
    public void run(ApplicationArguments args) {
//        userRepository.save(new User("zeljko", "zeljko"));

        Location loc1 = new Location("Maksimir", 45.818790, 16.012970);
        Location loc2 = new Location("Dinamo stadion", 45.828790, 16.032970);
        Location loc3 = new Location("Rokov perivoj", 45.814772, 15.967252);
        loc3.setDescription("Bolesna ulica");

        locationRepository.save(loc1);
        locationRepository.save(loc2);
        locationRepository.save(loc3);

        Route route = new Route();
        routeRepository.save(route);

        route.addLocation(loc1);
        route.addLocation(loc2);
        route.addLocation(loc3);

        routeRepository.save(route);

        Category sportCategory = new Category("Sport");
        categoryRepository.save(sportCategory);

        sportCategory.addRoute(route);

        categoryRepository.save(sportCategory);

        route.removeLocation(loc1);
        route.removeLocation(loc2);

        route.addLocation(new Location("Najjaca ulica", 45.718790, 16.112970));

        routeRepository.save(route);

        Location loc4 = new Location("Sljeme", 44.818790, 11.012970);
        Location loc5 = new Location("Trg bana Jelacica", 45.528790, 16.332970);
        Location loc6 = new Location("Mala ulica", 45.834772, 15.969252);

        locationRepository.save(loc4);
        locationRepository.save(loc5);
        locationRepository.save(loc6);

//        Category category2 = new Category("Opcenito");
//        categoryRepository.save(category2);

        Route route1 = new Route();
        routeRepository.save(route1);

        route1.addLocation(loc4);
        route1.addLocation(loc5);
        route1.addLocation(loc6);

//        category2.addRoute(route1);
        routeRepository.save(route1);

    }
}
