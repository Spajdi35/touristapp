package com.zeljkohalle.ExploreZAGREB.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "location")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "address")
    @NotNull
    private String address;

    @Column(name = "latitude")
    @NotNull
    private Double latitude;

    @Column(name = "longitude")
    @NotNull
    private Double longitude;

    @Column(name = "description")
    private String description;

    @NotNull
    private boolean granted = false;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "locations")
//    @JsonIgnore
    private Set<Route> routes;

    public Location() {
    }

    public Location(@NotNull String adress, @NotNull Double latitude, @NotNull Double longitude) {
        this.address = adress;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void addRoute(Route route) {
        if (routes == null) {
            routes = new HashSet<>();
        }

        routes.add(route);
    }

    public void removeRoute(Route route) {
        if (routes.contains(route) && route != null) {
            routes.remove(route);
        }
    }

    public Long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setAdress(String adress) {
        this.address = adress;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", adress='" + address + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", description='" + description + '\'' +
                '}';
    }

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }

}
