package com.zeljkohalle.ExploreZAGREB.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "category")
    private List<Route> routes;

    public Category() {
    }

    public Category(@NotNull String name) {
        this.name = name;
    }

    public Category(List<Route> routes) {
        this.routes = routes;
    }

    public Long getId() {
        return id;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public void addRoute(Route route) {
        if (routes == null) {
            routes = new ArrayList<>();
        }

        if (routes.contains(route)) {
            return;
        }

        route.setCategory(this);
        routes.add(route);
    }

    public void removeRoute(Route route) {
        if (routes.contains(route) && route != null) {
            route.setCategory(null);
            routes.remove(route);
        }
    }

}
