package com.zeljkohalle.ExploreZAGREB.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "route")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "granted")
    private boolean granted;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private Category category;

    @ManyToMany(cascade = CascadeType.ALL,
    fetch = FetchType.LAZY)
    @Column(name = "location")
    private List<Location> locations;

    public Route() {
    }

    public Route(@NotNull Category category, List<Location> locations) {
        this.category = category;
        this.locations = locations;
    }

    public Route(@NotNull Category category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public Category getCategory() {
        return category;
    }

    public List<Location> getLocations() {
        return locations;
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", locations=" + locations +
                '}';
    }

    public void addLocation(Location location) {
        if (locations == null) {
            locations = new ArrayList<>();
        }

        if (locations.contains(location)) {
            return;
        }

        location.addRoute(this);
        locations.add(location);
    }

    public void removeLocation(Location location) {
        if (locations.contains(location) && location != null) {
            location.removeRoute(this);
            locations.remove(location);
        }
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }
}
