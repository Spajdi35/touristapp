package com.zeljkohalle.ExploreZAGREB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExploreZagrebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExploreZagrebApplication.class, args);
	}

}
