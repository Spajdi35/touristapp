package com.zeljkohalle.ExploreZAGREB.rest;

import com.zeljkohalle.ExploreZAGREB.domain.Location;
import com.zeljkohalle.ExploreZAGREB.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @GetMapping("/getAll")
    public List<Location> getAll() {
        return locationService.listAll();
    }

    @PostMapping("/create")
    public Location createLocation(@RequestBody Location location) {
        return locationService.createLocation(location);
    }

    @GetMapping("/getAllGranted")
    public List<Location> getAllGranted() {
        return locationService.getAllByGranted(true);
    }

    @GetMapping("/getAllNonGranted")
    public List<Location> getAllNonGranted() {
        return locationService.getAllByGranted(false);
    }

    @PutMapping("/grant/{id}")
    public Location grantLocation(@PathVariable("id") Long id) {
        return locationService.grantLocation(id);
    }

}
