package com.zeljkohalle.ExploreZAGREB.rest;

import com.zeljkohalle.ExploreZAGREB.domain.User;
import com.zeljkohalle.ExploreZAGREB.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public List<User> getAll() {
        return userService.listAll();
    }

    @GetMapping("/getByEmail/{email}")
    public User getByEmail(@PathVariable("email") String email) {
        return userService.getByEmail(email);
    }

    @PostMapping
    public User registerUser(@RequestBody User user) {
        return userService.registerUser(user);
    }

}
