package com.zeljkohalle.ExploreZAGREB.rest;

import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/getAll")
    public List<Category> getAll() {
        return categoryService.getAll();
    }

    @GetMapping("/get/{name}")
    public Category getByName(@PathVariable("name") String name) {
        return categoryService.getByName(name);
    }

}
