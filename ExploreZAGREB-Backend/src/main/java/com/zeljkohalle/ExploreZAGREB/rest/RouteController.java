package com.zeljkohalle.ExploreZAGREB.rest;

import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Location;
import com.zeljkohalle.ExploreZAGREB.domain.Route;
import com.zeljkohalle.ExploreZAGREB.service.CategoryService;
import com.zeljkohalle.ExploreZAGREB.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/route")
public class RouteController {

    @Autowired
    private RouteService routeService;

    @GetMapping("/getAll")
    public List<Route> getAll() {
        return routeService.getAll();
    }

    @GetMapping("/getByCategoryId/{categoryId}")
    public List<Route> getAllByCategoryId(@PathVariable("categoryId") Long id) {
        return routeService.getAllByCategoryId(id);
    }

    @GetMapping("/getById/{id}")
    public Route getById(@PathVariable("id") Long id) {
        return routeService.getById(id);
    }

    @PutMapping("/addLocation/{routeId}/{locationId}")
    public Route addLocation(@PathVariable("routeId") Long routeId, @PathVariable("locationId") Long locationId) {
        return routeService.addLocation(routeId, locationId);
    }

    @PutMapping("/grant/{id}")
    public Route grantRoute(@PathVariable("id") Long id) {
        return routeService.grantRoute(id);
    }

}
