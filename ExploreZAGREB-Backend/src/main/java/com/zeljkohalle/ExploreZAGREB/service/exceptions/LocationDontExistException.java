package com.zeljkohalle.ExploreZAGREB.service.exceptions;

public class LocationDontExistException extends RuntimeException {

    public LocationDontExistException(String message) {
        super(message);
    }

}
