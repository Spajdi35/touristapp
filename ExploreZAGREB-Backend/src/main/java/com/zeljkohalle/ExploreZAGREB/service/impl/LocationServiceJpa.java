package com.zeljkohalle.ExploreZAGREB.service.impl;

import com.zeljkohalle.ExploreZAGREB.dao.LocationRepository;
import com.zeljkohalle.ExploreZAGREB.domain.Location;
import com.zeljkohalle.ExploreZAGREB.service.LocationService;
import com.zeljkohalle.ExploreZAGREB.service.exceptions.LocationAlreadyExistsException;
import com.zeljkohalle.ExploreZAGREB.service.exceptions.ResultNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class LocationServiceJpa implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public List<Location> listAll() {
        return locationRepository.findAll();
    }

    @Override
    public Location createLocation(Location location) {
        if (locationRepository.countByAddress(location.getAddress()) > 0) {
            throw new LocationAlreadyExistsException("Location with address " + location.getAddress() + " already exists");
        } else {
            Double latitude = location.getLatitude();
            Double longitude = location.getLongitude();
            if (locationRepository.countByLatitudeAndLongitude(latitude, longitude) > 0) {
                throw new LocationAlreadyExistsException("Location with latitude: " + latitude + ", longitude: " + longitude + " already exists");
            }
        }

        locationRepository.save(location);
        return location;
    }

    @Override
    public List<Location> getAllByGranted(boolean granted) {
        return locationRepository.findAllByGranted(granted).orElseThrow(
                () -> new ResultNotFoundException("No granted locations")
        );
    }

    @Override
    public Location grantLocation(Long id) {
        Location location = locationRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("No location with id " + id)
        );

        location.setGranted(true);
        locationRepository.save(location);
        return location;
    }
}
