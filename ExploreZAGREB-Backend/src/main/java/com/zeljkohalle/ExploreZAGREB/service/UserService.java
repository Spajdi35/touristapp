package com.zeljkohalle.ExploreZAGREB.service;

import com.zeljkohalle.ExploreZAGREB.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {
    public List<User> listAll();
    public User getByEmail(String email);
    public User registerUser(User user);
}
