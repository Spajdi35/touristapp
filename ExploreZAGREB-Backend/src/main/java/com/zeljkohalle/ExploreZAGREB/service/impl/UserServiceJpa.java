package com.zeljkohalle.ExploreZAGREB.service.impl;

import com.zeljkohalle.ExploreZAGREB.dao.UserRepository;
import com.zeljkohalle.ExploreZAGREB.domain.User;
import com.zeljkohalle.ExploreZAGREB.service.UserService;
import com.zeljkohalle.ExploreZAGREB.service.exceptions.ResultNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceJpa implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(
                () -> new ResultNotFoundException("User with email " + email + " does not exist")
        );
    }

    @Override
    public User registerUser(User user) {
        return userRepository.save(user);
    }

}
