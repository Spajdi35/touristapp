package com.zeljkohalle.ExploreZAGREB.service;

import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Route;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
    Category getByName(String name);
}
