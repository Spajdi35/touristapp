package com.zeljkohalle.ExploreZAGREB.service;

import com.zeljkohalle.ExploreZAGREB.domain.Location;

import java.util.List;

public interface LocationService {
    public List<Location> listAll();
    public Location createLocation(Location location);
    public List<Location> getAllByGranted(boolean granted);
    public Location grantLocation(Long id);
}
