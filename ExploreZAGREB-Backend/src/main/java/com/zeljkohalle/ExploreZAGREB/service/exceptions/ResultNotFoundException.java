package com.zeljkohalle.ExploreZAGREB.service.exceptions;

public class ResultNotFoundException extends RuntimeException {

    public ResultNotFoundException(String message) {
        super(message);
    }

}
