package com.zeljkohalle.ExploreZAGREB.service;


import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Location;
import com.zeljkohalle.ExploreZAGREB.domain.Route;

import java.util.List;
import java.util.Set;

public interface RouteService {
    public List<Route> getAll();
    public Route getById(Long id);
    public List<Route> getAllByCategoryId(Long id);
    public Route addLocation(Long routeId, Long locationId);
    public Route grantRoute(Long id);
}
