package com.zeljkohalle.ExploreZAGREB.service.impl;

import com.zeljkohalle.ExploreZAGREB.dao.CategoryRepository;
import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Route;
import com.zeljkohalle.ExploreZAGREB.service.CategoryService;
import com.zeljkohalle.ExploreZAGREB.service.exceptions.ResultNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceJpa implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.findByName(name).orElseThrow(
                () -> new ResultNotFoundException("No category named " + name)
        );
    }

}
