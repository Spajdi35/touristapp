package com.zeljkohalle.ExploreZAGREB.service.impl;

import com.zeljkohalle.ExploreZAGREB.dao.CategoryRepository;
import com.zeljkohalle.ExploreZAGREB.dao.LocationRepository;
import com.zeljkohalle.ExploreZAGREB.dao.RouteRepository;
import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Location;
import com.zeljkohalle.ExploreZAGREB.domain.Route;
import com.zeljkohalle.ExploreZAGREB.service.RouteService;
import com.zeljkohalle.ExploreZAGREB.service.exceptions.LocationAlreadyExistsException;
import com.zeljkohalle.ExploreZAGREB.service.exceptions.LocationDontExistException;
import com.zeljkohalle.ExploreZAGREB.service.exceptions.ResultNotFoundException;
import jdk.management.resource.ResourceRequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

@Service
public class RouteServiceJpa implements RouteService {

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public List<Route> getAll() {
        return routeRepository.findAll();
    }

    @Override
    public Route getById(Long id) {
        return routeRepository.findById(id).orElseThrow(
                () -> new ResultNotFoundException("Route with id " + id + " don't exist")
        );
    }

    @Override
    public List<Route> getAllByCategoryId(Long id) {
        return routeRepository.findAllByCategoryId(id).orElseThrow(
                () -> new ResultNotFoundException("No router with category id = " + id)
        );
    }

    @Override
    public Route addLocation(Long routeId, Long locationId) {
        Route route = routeRepository.findById(routeId).orElseThrow(
                () -> new ResultNotFoundException("No route with id " + routeId)
        );

        Location location = locationRepository.findById(locationId).orElseThrow(
                () -> new LocationDontExistException("Location with id " + locationId + " don't exist")
        );

        route.addLocation(location);
        routeRepository.save(route);
        return route;
    }

    @Override
    public Route grantRoute(Long id) {
        Route route = routeRepository.findById(id).orElseThrow(
                () -> new ResultNotFoundException("No route with id " + id +" found")
        );

        route.setGranted(true);
        routeRepository.save(route);
        return route;
    }
}
