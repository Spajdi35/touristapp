package com.zeljkohalle.ExploreZAGREB.dao;

import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    Optional<Category> findByName(String name);
}
