package com.zeljkohalle.ExploreZAGREB.dao;

import com.zeljkohalle.ExploreZAGREB.domain.Category;
import com.zeljkohalle.ExploreZAGREB.domain.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface RouteRepository extends JpaRepository<Route, Long> {
//    @Query("SELECT r FROM Route r WHERE category.id = :id") samo primjer ako ce mi trebat za kompleksnije upite
    Optional<List<Route>> findAllByCategoryId(Long id);
    Optional<Route> findById(Long id);
}
