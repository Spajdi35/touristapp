package com.zeljkohalle.ExploreZAGREB.dao;

import com.zeljkohalle.ExploreZAGREB.domain.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LocationRepository extends JpaRepository<Location, Long> {
    int countByAddress(String adress);
    int countByLatitudeAndLongitude(Double latitude, Double longitude);
    Optional<List<Location>> findAllByGranted(boolean granted);
}
